INSERT INTO address_types (id, code, name, business) VALUES
(1,'H','Home Address',false),
(2,'W','Work Address',true),
(3,'M','Mailing Address',false),
(4,'B','Billing Address',true);

INSERT INTO phone_types (id, code, name) VALUES
(1,'M','Mobile Phone'),
(2,'H','Home Phone'),
(3,'B','Business Phone');