package com.example.mysqlRest.model.enums;

public enum PaxStatus {
	ACTIVE("Active"), NOT_ACTIVATED("Not activated"),
	RULES_VIOLATION("Violation of the rules/Block an account"), READ_ONLY("Only for data holding"),
	DELETED("Deleted");

	private String label;

	private PaxStatus(String label) {
		this.label = label;
	}

	public String getId() {
		return this.name();
	}

	public String getName() {
		return this.getLabel();
	}

	public String getLabel() {
		return this.label;
	}
}
